package com.example.casestudyapp.interfaces

/**
 * Call back interface to switch to full screen fragment
 */
interface FullScreenCallBack {

    /**
     *  call back to switch view to full screen
     *
     *  @param data string data value
     *  @param type content type of the data
     */
    fun switchToFullScreen(data: String, type: String)
}