package com.example.casestudyapp.util

/**
 * enum for different data type or content of data that can be shared
 */
enum class DataType {
    Text, Image, Video, Images, Videos, Default, Error, Multiple
}