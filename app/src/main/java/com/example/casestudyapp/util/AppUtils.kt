package com.example.casestudyapp.util

import android.content.Context

/**
 * Utils class for common values and methods
 */
object AppUtils {
    /**
     * Method to check if a string is null and empty
     *
     * @param value string
     * @return boolean
     */
    fun isEmpty(value: String?): Boolean {
        return value == null || value.isEmpty()
    }

    /**
     * Method to check if a string is not null and empty
     *
     * @param value string
     * @return boolean
     */
    fun isNotEmpty(value: String?): Boolean {
        return value != null && value.isNotEmpty()
    }

    /**
     * Method to get the density threshold value
     *
     * @param context viewContext
     * @return int
     */
    fun getThreshold(context: Context): Int {
        return (context.resources.displayMetrics.density * AppConstants.GESTURE_THRESHOLD_DP + 0.5f).toInt()
    }

    object AppConstants {
        const val ONE = 1
        const val TWO = 2
        const val ZERO = 0
        const val SPLASH_TIMER: Long = 2500
        const val LAYOUT_HEIGHT = 18
        const val GESTURE_THRESHOLD_DP = 16.0f
        const val EMPTY_STRING = ""
        const val PNG_EXTENSION = ".png"
        const val JPG_EXTENSION = ".jpg"
        const val EXTENSION_3GP = ".3gp"
        const val MP4_EXTENSION = ".mp4"
        const val TEXT_PLAIN_DATA = "text/plain"
        const val IMAGE_EXTENSION = "image/"
        const val IMAGE = "image"
        const val VIDEO_EXTENSION = "video/"
        const val VIDEO = "video"
        const val KEY_CONTENT_TYPE = "Content type"
        const val DATA_KEY = "data"
        const val KEY_VIDEO_SEEK_POSITION = "Video seek position"
    }
}