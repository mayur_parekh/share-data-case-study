package com.example.casestudyapp.adapters

import android.net.Uri
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.casestudyapp.R
import com.example.casestudyapp.interfaces.FullScreenCallBack
import com.example.casestudyapp.ui.viewmodels.MainViewModel
import com.example.casestudyapp.util.AppUtils.AppConstants
import com.example.casestudyapp.util.AppUtils.getThreshold
import com.example.casestudyapp.util.AppUtils.isNotEmpty
import com.example.casestudyapp.util.DataType


class DataRecyclerAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var mData: String
    private lateinit var mType: DataType
    private lateinit var mContentList: ArrayList<Parcelable>
    private lateinit var mViewModel: MainViewModel
    private lateinit var mFullScreenCallBack: FullScreenCallBack

    constructor(
            callback: FullScreenCallBack,
            viewModel: MainViewModel,
            sharedText: String,
            type: DataType
    ) : this() {
        mFullScreenCallBack = callback
        mViewModel = viewModel
        mData = sharedText
        mType = type
    }

    constructor(
            callback: FullScreenCallBack,
            list: ArrayList<Parcelable>,
            type: DataType
    ) : this() {
        mFullScreenCallBack = callback
        mContentList = list
        mType = type
    }

    constructor(type: DataType) : this() {
        mType = type
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)

        viewHolder = when (viewType) {
            DataType.Text.ordinal, DataType.Default.ordinal, DataType.Image.ordinal, DataType.Video.ordinal -> {
                SingleViewHolder(inflater.inflate(R.layout.single_data_row, parent, false))
            }
            DataType.Images.ordinal, DataType.Videos.ordinal, DataType.Multiple.ordinal -> {
                MultiDataViewHolder(inflater.inflate(R.layout.multiple_data_row, parent, false))
            }
            else -> {
                SingleViewHolder(inflater.inflate(R.layout.single_data_row, parent, false))
            }
        }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return when (mType) {
            DataType.Text,
            DataType.Default,
            DataType.Error,
            DataType.Image,
            DataType.Video -> AppConstants.ONE
            DataType.Images, DataType.Multiple,
            DataType.Videos -> mContentList.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return mType.ordinal
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var type = AppConstants.EMPTY_STRING
        when (holder.itemViewType) {
            DataType.Text.ordinal -> {
                (holder as SingleViewHolder).sharedText.text = mData
                holder.sharedText.visibility = View.VISIBLE
            }
            DataType.Image.ordinal -> {
                (holder as SingleViewHolder).image.setImageURI(Uri.parse(mData))
                holder.image.visibility = View.VISIBLE

            }
            DataType.Video.ordinal -> {
                (holder as SingleViewHolder).video.visibility = View.VISIBLE
                val mediaController = MediaController(holder.video.context)
                mediaController.setAnchorView(holder.video)
                holder.video.setMediaController(mediaController)
                holder.video.setVideoURI(Uri.parse(mData))
                holder.video.requestFocus()
                holder.video.setOnPreparedListener {
                    mediaController.show(AppConstants.ZERO)
                }
            }
            DataType.Images.ordinal -> {
                (holder as MultiDataViewHolder).image.setImageURI(
                        mContentList[position] as Uri
                )
                holder.image.visibility = View.VISIBLE
                holder.textContainer.visibility = View.GONE
            }
            DataType.Videos.ordinal -> {
                (holder as MultiDataViewHolder).video.visibility = View.VISIBLE
                holder.textContainer.visibility = View.GONE
                holder.video.setVideoURI(Uri.parse((mContentList[position] as Uri).toString()))
                holder.video.rootView.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        getThreshold(holder.video.context) * AppConstants.LAYOUT_HEIGHT

                )
                holder.video.seekTo(AppConstants.ONE)
                holder.playButton.visibility = View.VISIBLE
            }

            DataType.Multiple.ordinal -> {
                val item = mContentList[position]
                if (item.toString().endsWith(AppConstants.PNG_EXTENSION) || item.toString().endsWith(AppConstants.JPG_EXTENSION)) {
                    (holder as MultiDataViewHolder).image.setImageURI(
                            item as Uri
                    )
                    holder.image.visibility = View.VISIBLE
                    holder.typeText.text = DataType.Image.name
                    holder.textContainer.visibility = View.VISIBLE
                    holder.image.rootView.layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            getThreshold(holder.image.context) * AppConstants.LAYOUT_HEIGHT
                    )
                    type = DataType.Image.name
                } else if (item.toString().endsWith(AppConstants.MP4_EXTENSION) || item.toString()
                                .endsWith(AppConstants.EXTENSION_3GP)
                ) {
                    (holder as MultiDataViewHolder).video.visibility = View.VISIBLE
                    holder.video.setVideoURI(Uri.parse((item as Uri).toString()))
                    holder.playButton.visibility = View.VISIBLE
                    holder.video.setOnPreparedListener {
                        holder.video.seekTo(holder.video.duration / AppConstants.TWO)
                    }
                    holder.typeText.text = DataType.Video.name
                    holder.textContainer.visibility = View.VISIBLE
                    holder.video.rootView.layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            getThreshold(holder.video.context) * AppConstants.LAYOUT_HEIGHT

                    )
                    type = DataType.Video.name
                }
            }
            DataType.Default.ordinal -> {
                (holder as SingleViewHolder).sharedText.text =
                        holder.sharedText.context.getString(R.string.welcome_string)
                holder.sharedText.visibility = View.VISIBLE
            }
            DataType.Error.ordinal -> {
                (holder as SingleViewHolder).sharedText.text =
                        holder.sharedText.context.getString(R.string.error_string)
                holder.sharedText.visibility = View.VISIBLE
            }
        }

        when (holder) {
            is SingleViewHolder -> {
                if (holder.itemViewType != DataType.Error.ordinal && holder.itemViewType != DataType.Default.ordinal)
                    holder.sharedViewContainer.setOnClickListener {
                        mFullScreenCallBack.switchToFullScreen(mData, DataType.values()[holder.itemViewType].name)
                    }
            }
            is MultiDataViewHolder -> holder.sharedViewContainer.setOnClickListener {
                if (holder.itemViewType == DataType.Multiple.ordinal && isNotEmpty(type))
                    mFullScreenCallBack.switchToFullScreen(mContentList[position].toString(), type)
                else {
                    mFullScreenCallBack.switchToFullScreen(mContentList[position].toString(), DataType.values()[holder.itemViewType].name)
                }
            }
        }
    }

}


class SingleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var image: ImageView = itemView.findViewById(R.id.shared_image)
    var sharedText: TextView = itemView.findViewById(R.id.shared_text)
    var video: VideoView = itemView.findViewById(R.id.shared_video)
    var sharedViewContainer: RelativeLayout = itemView.findViewById(R.id.shared_view_container)
}


class MultiDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var image: ImageView = itemView.findViewById(R.id.shared_image)
    var video: VideoView = itemView.findViewById(R.id.shared_video)
    var textContainer: LinearLayout = itemView.findViewById(R.id.text_container)
    var typeText: TextView = itemView.findViewById(R.id.text_type)
    val playButton: ImageView = itemView.findViewById(R.id.play_button)
    var sharedViewContainer: RelativeLayout = itemView.findViewById(R.id.shared_view_container)
}

