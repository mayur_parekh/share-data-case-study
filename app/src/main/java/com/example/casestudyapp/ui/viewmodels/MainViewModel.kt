package com.example.casestudyapp.ui.viewmodels

import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.casestudyapp.adapters.DataRecyclerAdapter
import com.example.casestudyapp.interfaces.FullScreenCallBack
import com.example.casestudyapp.ui.fragments.MainFragment
import com.example.casestudyapp.util.AppUtils
import com.example.casestudyapp.util.DataType
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class MainViewModel : ViewModel() {

    private var mAdapter: DataRecyclerAdapter? = null
    private var mIsIntentValid = false
    private var mData: String = AppUtils.AppConstants.EMPTY_STRING
    private var mDataType: String = AppUtils.AppConstants.EMPTY_STRING
    private var mTextVisible = MutableLiveData(false)
    private lateinit var mCallback: FullScreenCallBack
    private var mVideoSeekPosition = AppUtils.AppConstants.ZERO

    /**
     * Method to handle intent data
     * @param callBack fullscreen Callback
     * @param intent Intent
     * @param contentResolver Content Resolver
     */
    fun handleIntent(
            callBack: FullScreenCallBack,
            intent: Intent,
            contentResolver: ContentResolver
    ) {
        mCallback = callBack
        when (intent.action) {
            Intent.ACTION_SEND -> {
                if (AppUtils.AppConstants.TEXT_PLAIN_DATA == intent.type) {
                    mDataType = DataType.Text.name
                    handleSendText(intent, contentResolver)
                } else if (intent.type?.startsWith(AppUtils.AppConstants.IMAGE_EXTENSION) == true || intent.type?.startsWith(AppUtils.AppConstants.VIDEO_EXTENSION) == true) {
                    handleSendImageVideo(intent)
                }
            }
            Intent.ACTION_SEND_MULTIPLE
            -> {
                handleSendMultipleData(intent)
            }
        }
    }

    /**
     *  Function to Handle text being sent
     *  @param intent
     *  @param contentResolver
     */
    private fun handleSendText(
            intent: Intent,
            contentResolver: ContentResolver
    ) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            mAdapter = DataRecyclerAdapter(mCallback, this, it, DataType.Text)
            mData = it
            mIsIntentValid = true
        }
        val uri = intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM)
        if (uri != null)
            try {
                val `in`: InputStream? = contentResolver.openInputStream(Uri.parse(uri.toString()))
                val r = BufferedReader(InputStreamReader(`in`))
                val total = StringBuilder()
                var line = r.readLine()
                while (line != null) {
                    total.append(line).append('\n')
                    line = r.readLine()
                }
                mAdapter = DataRecyclerAdapter(mCallback, this, total.toString(), DataType.Text)
                mData = total.toString()
                mIsIntentValid = true

            } catch (e: Exception) {
                e.message?.let { it1 -> Log.d(MainFragment.toString(), it1) }
                mIsIntentValid = false
            }

    }

    /**
     * Function to  Handle single image being sent
     * @param intent
     */
    private fun handleSendImageVideo(intent: Intent) {
        (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri)?.let {
            mAdapter =
                    if (intent.type?.startsWith(AppUtils.AppConstants.IMAGE_EXTENSION) == true || intent.type?.contains(AppUtils.AppConstants.IMAGE) == true) {
                        mDataType = DataType.Image.name
                        DataRecyclerAdapter(mCallback, this, it.toString(), DataType.Image)
                    } else {
                        mDataType = DataType.Video.name
                        DataRecyclerAdapter(mCallback, this, it.toString(), DataType.Video)
                    }
            mData = it.toString()
            mIsIntentValid = true
        }
    }

    /**
     * Method to Handle multiple images being sent
     * @param intent
     */
    private fun handleSendMultipleData(intent: Intent) {
        intent.getParcelableArrayListExtra<Parcelable>(Intent.EXTRA_STREAM)?.let {
            if (intent.type?.startsWith(AppUtils.AppConstants.IMAGE_EXTENSION) == true || intent.type?.contains(AppUtils.AppConstants.IMAGE) == true) {
                mAdapter = DataRecyclerAdapter(mCallback, it, DataType.Images)
                mDataType = DataType.Images.name
                mIsIntentValid = true
            } else if (intent.type?.startsWith(AppUtils.AppConstants.VIDEO_EXTENSION) == true || intent.type?.contains(AppUtils.AppConstants.VIDEO) == true) {
                mAdapter = DataRecyclerAdapter(mCallback, it, DataType.Videos)
                mDataType = DataType.Videos.name
                mIsIntentValid = true
            } else {
                mAdapter = DataRecyclerAdapter(mCallback, it, DataType.Multiple)
                mIsIntentValid = true
                mDataType = DataType.Multiple.name
            }
        }
    }

    fun getAdapter(): DataRecyclerAdapter {
        return if (!mIsIntentValid) {
            mTextVisible.value = false
            DataRecyclerAdapter(DataType.Default)
        } else if (mAdapter != null) {
            mTextVisible.value = true
            mAdapter as DataRecyclerAdapter
        } else {
            mTextVisible.value = false
            DataRecyclerAdapter(DataType.Error)
        }

    }

    fun getData(): String {
        return mData
    }

    fun getContentType(): String {
        return mDataType
    }

    fun getTextVisibility(): MutableLiveData<Boolean> {
        return mTextVisible
    }

    fun getVideoSeekPosition(): Int {
        return mVideoSeekPosition
    }

}