package com.example.casestudyapp.ui.fragments


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.casestudyapp.R
import com.example.casestudyapp.adapters.DataRecyclerAdapter
import com.example.casestudyapp.databinding.MainFragmentBinding
import com.example.casestudyapp.interfaces.FullScreenCallBack
import com.example.casestudyapp.ui.activities.MainActivity
import com.example.casestudyapp.ui.viewmodels.MainViewModel
import com.example.casestudyapp.util.AppUtils
import com.example.casestudyapp.util.AppUtils.AppConstants
import com.example.casestudyapp.util.AppUtils.isNotEmpty
import com.example.casestudyapp.util.DataType


class MainFragment : Fragment(),
        FullScreenCallBack {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var mViewModel: MainViewModel
    private lateinit var mainFragmentBinding: MainFragmentBinding

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        mainFragmentBinding = MainFragmentBinding.inflate(inflater, container, false)
        return mainFragmentBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mainFragmentBinding.lifecycleOwner = this
        mainFragmentBinding.mainViewModel = mViewModel
        var contentType = AppConstants.EMPTY_STRING
        var data = AppConstants.EMPTY_STRING
        if (savedInstanceState == null) {
            activity?.intent?.let {
                context?.contentResolver?.let { it1 ->
                    mViewModel.handleIntent(
                            this@MainFragment,
                            it,
                            it1
                    )
                }
            }
        } else {
            contentType = savedInstanceState.getString(AppConstants.KEY_CONTENT_TYPE, AppConstants.EMPTY_STRING)
            data = savedInstanceState.getString(AppConstants.DATA_KEY, AppConstants.EMPTY_STRING)
        }
        val recyclerView = view.findViewById<RecyclerView>(R.id.main_recycler_view)
        if (activity?.intent?.action == Intent.ACTION_SEND_MULTIPLE) {
            recyclerView.layoutManager =
                    StaggeredGridLayoutManager(AppConstants.TWO, StaggeredGridLayoutManager.VERTICAL)

        } else {
            recyclerView.layoutManager = LinearLayoutManager(context)
        }
        if (savedInstanceState == null || AppUtils.isEmpty(contentType) || AppUtils.isEmpty(contentType))
            recyclerView.adapter = mViewModel.getAdapter()
        else {
            recyclerView.adapter =
                    DataRecyclerAdapter(this, mViewModel, data, DataType.valueOf(contentType))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (isNotEmpty(mViewModel.getContentType()) && isNotEmpty(mViewModel.getData())) {
            outState.putString(AppConstants.KEY_CONTENT_TYPE, mViewModel.getContentType())
            outState.putString(AppConstants.DATA_KEY, mViewModel.getData())
        }
    }

    override fun switchToFullScreen(data: String, type: String) {
        val fullScreenFragment = FullScreenFragment()
        val args = Bundle()
        if (isNotEmpty(data) && isNotEmpty(type)) {
            args.putString(AppConstants.KEY_CONTENT_TYPE, type)
            args.putString(AppConstants.DATA_KEY, data)
            args.putInt(AppConstants.KEY_VIDEO_SEEK_POSITION, mViewModel.getVideoSeekPosition())
            fullScreenFragment.arguments = args
            (activity as MainActivity).moveToNext(fullScreenFragment)
        }
    }


}