package com.example.casestudyapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.casestudyapp.R
import com.example.casestudyapp.ui.activities.MainActivity
import com.example.casestudyapp.util.AppUtils
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


class SplashFragment : Fragment(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.findViewById<TextView>(R.id.loading_textView).startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.blink))
        if (activity != null)
            launch {
                delay(AppUtils.AppConstants.SPLASH_TIMER)
                withContext(Dispatchers.Main) {
                    (activity as MainActivity).moveToNext(MainFragment.newInstance())
                }
            }
    }

    companion object {
        fun newInstance() = SplashFragment()
    }
}