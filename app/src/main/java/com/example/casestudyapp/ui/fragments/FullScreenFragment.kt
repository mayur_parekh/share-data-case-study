package com.example.casestudyapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.casestudyapp.databinding.FullScreenFragmentBinding
import com.example.casestudyapp.ui.viewmodels.FullScreenViewModel
import com.example.casestudyapp.util.AppUtils.AppConstants
import com.example.casestudyapp.util.DataType

class FullScreenFragment : Fragment() {

    private lateinit var mFullScreenViewModel: FullScreenViewModel
    private lateinit var mFullScreenFragmentBinding: FullScreenFragmentBinding
    private lateinit var mType: String
    private var mVideoSeekPosition = AppConstants.ZERO

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mFullScreenFragmentBinding = FullScreenFragmentBinding.inflate(inflater, container, false)
        return mFullScreenFragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mFullScreenViewModel = ViewModelProvider(this).get(FullScreenViewModel::class.java)
        mFullScreenFragmentBinding.lifecycleOwner = this
        mFullScreenFragmentBinding.mViewModel = mFullScreenViewModel
        mFullScreenViewModel.init(mFullScreenFragmentBinding)
        mType = requireArguments().getString(AppConstants.KEY_CONTENT_TYPE)!!
        mFullScreenViewModel.setData(
                arguments?.getString(AppConstants.DATA_KEY)!!,
                mType,
                requireArguments().getInt(AppConstants.KEY_VIDEO_SEEK_POSITION)
        )
        mFullScreenFragmentBinding.fullVideoView.setOnCompletionListener {
            activity?.supportFragmentManager?.popBackStack()
        }
    }

    override fun onResume() {
        mFullScreenFragmentBinding.fullVideoView.setOnPreparedListener {
            mFullScreenFragmentBinding.fullVideoView.seekTo(mVideoSeekPosition + AppConstants.TWO)
            mFullScreenFragmentBinding.fullVideoView.start()
        }
        super.onResume()
    }

    override fun onPause() {
        if (mType == DataType.Video.name) {
            mFullScreenFragmentBinding.fullVideoView.pause()
            mVideoSeekPosition = mFullScreenFragmentBinding.fullVideoView.currentPosition
        }
        super.onPause()
    }
}