package com.example.casestudyapp.ui.viewmodels

import android.net.Uri
import android.text.SpannableString
import android.widget.MediaController
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.casestudyapp.databinding.FullScreenFragmentBinding
import com.example.casestudyapp.util.AppUtils
import com.example.casestudyapp.util.DataType

class FullScreenViewModel : ViewModel() {

    private lateinit var mTextData: MutableLiveData<SpannableString>
    private lateinit var mTextVisibility: MutableLiveData<Boolean>
    private lateinit var mImageVisibility: MutableLiveData<Boolean>
    private lateinit var mVideoVisibility: MutableLiveData<Boolean>
    private lateinit var mBinding: FullScreenFragmentBinding

    /**
     * Method to initialize the view mode data
     * @param binding binding object
     */
    fun init(binding: FullScreenFragmentBinding) {
        mBinding = binding
        mTextData = MutableLiveData(SpannableString(AppUtils.AppConstants.EMPTY_STRING))
        mTextVisibility = MutableLiveData(false)
        mImageVisibility = MutableLiveData(false)
        mVideoVisibility = MutableLiveData(false)
    }

    fun getTextVisibility(): MutableLiveData<Boolean> {
        return mTextVisibility
    }

    fun getImageVisibility(): MutableLiveData<Boolean> {
        return mImageVisibility
    }

    fun getVideoVisibility(): MutableLiveData<Boolean> {
        return mVideoVisibility
    }

    fun getTextData(): MutableLiveData<SpannableString> {
        return mTextData
    }

    /**
     * Method to set data based on the type parameter
     * @param data string data to be processed
     * @param type of the data
     * @param videoSeekPosition current video seek position
     */
    fun setData(
            data: String,
            type: String,
            videoSeekPosition: Int
    ) {
        when (type) {
            DataType.Text.name -> {
                mTextData.value = SpannableString.valueOf(data)
                setVisibility(
                        textVisibility = true,
                        imageVisibility = false,
                        videoVisibility = false
                )
            }
            DataType.Image.name, DataType.Images.name -> {
                mBinding.fullImageView.setImageURI(Uri.parse(data))
                setVisibility(
                        textVisibility = false,
                        imageVisibility = true,
                        videoVisibility = false
                )
            }
            DataType.Video.name, DataType.Videos.name -> {
                mBinding.fullVideoView.setVideoURI(Uri.parse(data))
                val mediaController = MediaController(mBinding.root.context)
                mediaController.setAnchorView(mBinding.fullVideoView)
                mBinding.fullVideoView.setMediaController(mediaController)
                mBinding.fullVideoView.requestFocus()
                mBinding.fullVideoView.setOnPreparedListener {
                    mBinding.fullVideoView.seekTo(videoSeekPosition)
                    mBinding.fullVideoView.start()
                }
                setVisibility(
                        textVisibility = false,
                        imageVisibility = false,
                        videoVisibility = true
                )
            }
        }
    }


    /**
     * Method to set visibility of the views
     * @param imageVisibility boolean to set image visibility
     * @param textVisibility boolean to set text visibility
     * @param videoVisibility boolean to set video visibility
     */
    private fun setVisibility(
            textVisibility: Boolean,
            imageVisibility: Boolean,
            videoVisibility: Boolean
    ) {
        mTextVisibility.value = textVisibility
        mImageVisibility.value = imageVisibility
        mVideoVisibility.value = videoVisibility
    }
}