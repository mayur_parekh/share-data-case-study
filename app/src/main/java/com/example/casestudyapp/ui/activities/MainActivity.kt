package com.example.casestudyapp.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.casestudyapp.R
import com.example.casestudyapp.ui.fragments.FullScreenFragment
import com.example.casestudyapp.ui.fragments.SplashFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, SplashFragment.newInstance())
                    .commitNow()
        }
    }


    /**
     * Method to move to next fragment
     * @param fragment to which to be transact
     */
    fun moveToNext(fragment: Fragment) {
        if (fragment is FullScreenFragment)
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(
                            R.anim.slide_in,
                            R.anim.slide_out_left
                    )
                    .replace(R.id.container, fragment).addToBackStack(null)
                    .commit()
        else
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(
                            R.anim.slide_up_bottom, 0)
                    .replace(R.id.container, fragment)
                    .commit()
    }


}